import re

def is_valid_IP(string):
    ipRegex = re.compile(r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
    check = ipRegex.match(string)
    if check:
        return True
    else:
        return False


is_valid_IP()
