import re
from colormap import rgb2hex

def rgb(r, g, b):
    r = str(r)
    g = str(g)
    b = str(b)
    color = [r, g, b]
    new_color = []
    """Validate RGB format"""
    rgbRegex =  re.compile(r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))$")
    for x in color:
        """If out of range, round to the closest possible"""
        if not rgbRegex.match(x):
            check = re.compile(r'^-')
            if check.match(x):
                switch =rgbRegex.sub('0', '0')
                new_color.append(switch)
            else:
                switch = rgbRegex.sub("255", "255")
                new_color.append(switch)
        else:
            new_color.append(x)

    list = []
    for x in new_color:
        y = int(x)
        list.append(y)
    """Swith to hex format"""
    work = rgb2hex(list[0], list[1], list[2])
    print(work)


rgb(900,255,3)
